import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.firefox.FirefoxDriver as FirefoxDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor
import com.kms.katalon.core.webui.driver.firefox.CGeckoDriver as CGeckoDriver
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty
import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper as MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.exception.WebElementNotFoundException as WebElementNotFoundException
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

WebUI.click(findTestObject('Object Repository/pruebas/Page_Think20labs LIMS/a_Pre Test Preparation'))

WebUI.click(findTestObject('Object Repository/pruebas/Page_Think20labs LIMS/span_COC'))

/*WebUI.click(findTestObject('asdf/Page_Think20labs LIMS/button_Create COC - Copy'))*/
WebUI.delay(2)
WebUI.click(findTestObject('asdf/Page_Think20labs LIMS/button_Create COC - Copy'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/pruebas/Page_Think20labs LIMS/input_Customer Name_inpContact'), 'Fatima')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/pruebas/Page_Think20labs LIMS/input_Phone number_inpPhoneNum'), '(664) 440-2478')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/pruebas/Page_Think20labs LIMS/input_Email_inpEmail'), 'fruiz@biosoftinc.ocm')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/pruebas/Page_Think20labs LIMS/input_Business Name_inpBusName'), 'Biosoft Baja')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/pruebas/Page_Think20labs LIMS/input_Business Address_inpBusAddrs'), 'Loma Dorada')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/pruebas/Page_Think20labs LIMS/input_City_inpCity'), 'TIJUANA')

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/pruebas/Page_Think20labs LIMS/select_Select StateALAKAZARCACOCTDEDCFLGAHIIDILINIAKSKYLAMEMDMAMIMNMSMOMTNENVNHNJNMNYNCNDOHOKORPARISCSDTNTXUTVTVAWAWVWIWY'), 
    'ME', true)

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/pruebas/Page_Think20labs LIMS/input_ZipCode_inpZip'), '22207')

WebUI.delay(2)

WebUI.click(findTestObject('pruebas/Page_Think20labs LIMS/button_Add Comments'))

WebUI.delay(2)

WebUI.setText(findTestObject('pruebas/Page_Think20labs LIMS/textarea_Add Comments_comments'), 'COC EXAMPLE')

WebUI.delay(2)

WebUI.click(findTestObject('pruebas/Page_Think20labs LIMS/button_OK'))

WebUI.delay(5)

WebUI.click(findTestObject('asdf/Page_Think20labs LIMS/button_Save'))

/*WebUI.sendKeys(findTestObject(copiar), Keys.chord(Keys.CONTROL, 'a'))*/
//Read text from an input field where the text is disabled
/*WebDriver driver = new FirefoxDriver();*/
/*WebElement element = WebUiCommonHelper.findWebElement(to, timeout)*/
WebUI.delay(5)

WebUI.closeBrowser()

