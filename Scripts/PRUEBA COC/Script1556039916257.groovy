import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.firefox.FirefoxDriver as FirefoxDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor
import com.kms.katalon.core.webui.driver.firefox.CGeckoDriver as CGeckoDriver
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty
import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper as MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.exception.WebElementNotFoundException as WebElementNotFoundException
import com.kms.katalon.core.annotation.Keyword as Keyword
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger

//import com.kms.katalon.core.testobject.TestObject as TestObject
//import com.kms.katalon.core.testobject.ConditionType
//WebUI.click(findTestObject('asdf/Page_Think20labs LIMS/button_Save'))
/*WebUI.sendKeys(findTestObject(copiar), Keys.chord(Keys.CONTROL, 'a'))*/
WebUI.waitForPageLoad(24)

ready = WebUI.verifyElementVisible(findTestObject('asdf/Page_Think20labs LIMS/td_COC0000121'), FailureHandling.CONTINUE_ON_FAILURE)

while (!(ready)) {
    WebUI.delay(0.5)

    ready = WebUI.verifyElementVisible(findTestObject('asdf/Page_Think20labs LIMS/td_COC0000121'), FailureHandling.CONTINUE_ON_FAILURE)
}

WebDriver driver = DriverFactory.getWebDriver()
WebElement Webtable = driver.findElement(By.id('tableCOCData'))
List<WebElement> TotalRowCount = Webtable.findElements(By.xpath('//*[@id=\'tableCOCData\']/tbody/tr'))

for (int loop = 1; loop <= TotalRowCount.size(); loop++) 
{
    String COCOBTENIDO = driver.findElement(By.xpath('//*[@id=\'tableCOCData\']/tbody/tr[1]/td[2]')).getText()
    String copiandococ = driver.findElement(By.xpath('//input[@class=\'form-control input-sm\']')).sendKeys(COCOBTENIDO)
	WebUI.sendKeys(findTestObject('//input[@class=\'form-control input-sm\']'), Keys.chord(Keys.CONTROL, 'a'))
	WebUI.sendKeys(findTestObject('//input[@class=\'form-control input-sm\']'), Keys.chord(Keys.CONTROL, 'c'))
}


WebUI.click(findTestObject('AS/Page_Think20labs LIMS/a_Manage Batch'))
WebUI.click(findTestObject('Object Repository/AS/Page_Think20labs LIMS/button_Accession New Batch'))

WebUI.setText(findTestObject('Object Repository/AS/Page_Think20labs LIMS/input_Sample Id_inpSampleID'), 'SMP0000896')
WebUI.sendKeys(findTestObject('Object Repository/AS/Page_Think20labs LIMS/input_Sample Id_inpSampleID'), Keys.chord(Keys.CONTROL, 'a'))
WebUI.sendKeys(findTestObject('Object Repository/AS/Page_Think20labs LIMS/input_Sample Id_inpSampleID'), Keys.chord(Keys.CONTROL, 'c'))
WebUI.selectOptionByValue(findTestObject('Object Repository/AS/Page_Think20labs LIMS/select_Select a categoryBotanicalConcentratesEdiblesIsolatesTopicals'),'Concentrates', true)
WebUI.selectOptionByValue(findTestObject('Object Repository/AS/Page_Think20labs LIMS/select_Select a sub-categoryBHOsCO2DistillateHashKielRSOVape Pens'),'CO2', true)
WebUI.setText(findTestObject('Object Repository/AS/Page_Think20labs LIMS/input_Batch Weight_inpBatchWeight'), '100.00')
WebUI.selectOptionByValue(findTestObject('Object Repository/AS/Page_Think20labs LIMS/select_gkglmlul'), 'kg', true)
WebUI.setText(findTestObject('Object Repository/AS/Page_Think20labs LIMS/input_Combined Sample Weight_inpCombinedSampleWeight'),'20.00')
WebUI.selectOptionByValue(findTestObject('Object Repository/AS/Page_Think20labs LIMS/select_gkglmlul_1'), 'g', true)
WebUI.click(findTestObject('Object Repository/AS/Page_Think20labs LIMS/button_Save'))
WebUI.click(findTestObject('Object Repository/AS/Page_Think20labs LIMS/button_Save'))


WebUI.waitForPageLoad(4)

WebUI.callTestCase(findTestCase('mANAGE bATCH'), [:], FailureHandling.STOP_ON_FAILURE)

