import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Object Repository/sa/Page_Think20labs LIMS/span_Foreign Visual Analysis'))

WebUI.setText(findTestObject('Object Repository/sa/Page_Think20labs LIMS/input_Sample ID_sample'), 'asdasdasda')

WebUI.setText(findTestObject('Object Repository/sa/Page_Think20labs LIMS/input_Instrument Type_instrumentType'), 'asdasdasd')

WebUI.setText(findTestObject('Object Repository/sa/Page_Think20labs LIMS/input_Instrument Barcode_instrumentBarcode'), 'asdsdaasda')

WebUI.click(findTestObject('Object Repository/sa/Page_Think20labs LIMS/button_ADD COMMENTS'))

WebUI.setText(findTestObject('Object Repository/sa/Page_Think20labs LIMS/textarea_Add Comments_comment'), 'asdas')

WebUI.click(findTestObject('Object Repository/sa/Page_Think20labs LIMS/button_OK'))

WebUI.click(findTestObject('Object Repository/sa/Page_Think20labs LIMS/input_Instrument Barcode_Verify'))