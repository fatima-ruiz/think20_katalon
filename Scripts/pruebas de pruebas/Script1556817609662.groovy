import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://10.2.1.8:8080/Think20/')

WebUI.setText(findTestObject('Page_Think20labs LIMS Login/input_LIMS Log in_username'), 'linx')

WebUI.setEncryptedText(findTestObject('Page_Think20labs LIMS Login/input_LIMS Log in_password'), 'a7FufTyysRA=')

WebUI.click(findTestObject('Page_Think20labs LIMS Login/button_Log in'))

WebUI.click(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/a_Pre Test Preparation'))

WebUI.click(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/span_COC'))

WebUI.click(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/a_Manage Batch'))

WebUI.click(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/a_Edit'))

WebUI.click(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/a_Verify'))

WebUI.setText(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/input_User_inpVerifyUser'), 'daniboy')

WebUI.setEncryptedText(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/input_Password_inpVerifyPassword'), 
    '4gNQp31NWaY=')

WebUI.click(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/button_Verify'))

WebUI.click(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/a_Verify'))

WebUI.setText(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/input_User_inpVerifyUser'), 'daniboy')

WebUI.setEncryptedText(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/input_Password_inpVerifyPassword'), 
    '4gNQp31NWaY=')

WebUI.click(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/button_Verify'))

WebUI.click(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/a_Upload Image'))

WebUI.closeBrowser()

