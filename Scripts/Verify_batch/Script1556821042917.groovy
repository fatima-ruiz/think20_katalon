import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.sendKeys(findTestObject('Object Repository/AS/Page_Think20labs LIMS/input_Sample Id_inpSampleID'), Keys.chord(Keys.CONTROL, 'a'))
WebUI.sendKeys(findTestObject('Object Repository/AS/Page_Think20labs LIMS/input_Sample Id_inpSampleID'), Keys.chord(Keys.CONTROL, 'c'))

WebUI.click(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/a_Verify'))
WebUI.setText(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/input_User_inpVerifyUser'), 'daniboy')
WebUI.setText(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/input_User_inpVerifyUser'), 'daniboy')
WebUI.setEncryptedText(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/input_Password_inpVerifyPassword'), '4gNQp31NWaY=')
WebUI.click(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/button_Verify'))

WebUI.click(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/a_Verify'))
WebUI.setText(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/input_User_inpVerifyUser'), 'daniboy')
WebUI.setText(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/input_User_inpVerifyUser'), 'daniboy')
WebUI.setEncryptedText(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/input_Password_inpVerifyPassword'), '4gNQp31NWaY=')
WebUI.click(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/button_Verify'))

WebUI.click(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/a_Verify'))
WebUI.setText(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/input_User_inpVerifyUser'), 'daniboy')
WebUI.setText(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/input_User_inpVerifyUser'), 'daniboy')
WebUI.setEncryptedText(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/input_Password_inpVerifyPassword'), '4gNQp31NWaY=')
WebUI.click(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/button_Verify'))

WebUI.click(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/a_Verify'))
WebUI.setText(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/input_User_inpVerifyUser'), 'daniboy')
WebUI.setText(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/input_User_inpVerifyUser'), 'daniboy')
WebUI.setEncryptedText(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/input_Password_inpVerifyPassword'), '4gNQp31NWaY=')
WebUI.click(findTestObject('Object Repository/pruebas de pruebas/Page_Think20labs LIMS/button_Verify'))

WebUI.callTestCase(findTestCase('IMAGE_UPLOAD'), [:], FailureHandling.STOP_ON_FAILURE)
